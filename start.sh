#!/bin/sh
# USAGE: starts shotgun thin webserver for local development for Mac OSX.
# Pass in the adapter (e.g. ./start.sh en0)
NETWORK_ADAPTER="$1"
IPADDRESS=`ipconfig getifaddr $NETWORK_ADAPTER`
shotgun -s thin -p 4200 -o $IPADDRESS app.rb