var ROW_Y_START = 90,
    ROW_Y_END = 135,
    REM_Y_START = 787,
    REM_Y_END = 828,
    PC_Y_START = 226,
    PC_Y_END = 291,
    WTB_Y_START = 291,
    WTB_Y_END = 370,
    RL_Y_START = 444,
    RL_Y_END = 45

var animationCurve = "spring(600,40,500)";

var directionsForm = PSD.directions_form,
    directionsLink = PSD.directions_link,
    dlArrowUp = PSD.dl_arrow_up_r1,
    textForm = PSD.text_form,
    textLink = PSD.text_link,
    tlArrowUp = PSD.tl_arrow_up_r1,
    resultsRemaining = PSD.results_remaining,
    mapQuest = PSD.mapquest,
    categoriesLink = PSD.show_categories,
    scArrowUp = PSD.sc_arrow_up,
    prodCategories = PSD.product_categories,
    whereToBuy = PSD.where_to_buy,
    resultList = PSD.result_list,
    hideMapLink = PSD.hide_map_link,
    hmArrowUp = PSD.hm_arrow_up,
    map = PSD.map_loading,
    mapPinDialog = PSD.map_pin_dialog,
    pin = PSD.pin_1,
    closePinDialog = PSD.close_pin_dialog,
    pinDirectionsLink = PSD.pin_directions_link,
    pinTextLink = PSD.pin_text_link,
    pinTextForm = PSD.pin_text_address,
    pinDirectionsForm = PSD.pin_directions_form,
    activeGD = PSD.active_gd,
    activeTA = PSD.active_ta,
    state = 'default';

directionsForm.y = ROW_Y_START;
textForm.y = ROW_Y_START;
resultsRemaining.y = REM_Y_START;
mapQuest.opacity = 0;
prodCategories.y = PC_Y_START;
whereToBuy.y = WTB_Y_START;
hmArrowUp.rotation = 180;
mapPinDialog.opacity = 0;
activeGD.opacity = 0;
activeTA.opacity = 1;
pinTextForm.opacity = 0;

utils.delay(1000, function() {
	mapQuest.animate({
		properties: { opacity:1 }
	});
});

pinTextLink.on("click", function() {
  activeGD.opacity = 1;
  activeTA.opacity = 0;
  pinDirectionsForm.opacity = 0;
  pinTextForm.opacity = 1;
});

pinDirectionsLink.on("click", function() {
  activeGD.opacity = 0;
  activeTA.opacity = 1;
  pinTextForm.opacity = 0;
  pinDirectionsForm.opacity = 1;
});

closePinDialog.on("click", function() {
  mapPinDialog.opacity = 0;
});

pin.on("click", function() {
  mapPinDialog.opacity = 1;
});

hideMapLink.on("click", function() {

  if (map.opacity === 0) {
    opacity = 1;
    endY = RL_Y_START;
    hmArrowUp.rotation = 180;
  }else {
    opacity = 0;
    endY = RL_Y_END;
    hmArrowUp.rotation = 0;
  }

  resultList.animate({
    properties: { y:endY },
    curve: animationCurve
  });  

  map.animate({
    properties: { opacity:opacity }
  });

});

categoriesLink.on("click", function() {

  if (prodCategories.y === PC_Y_START) {
    endY = PC_Y_END;
    remEndY = WTB_Y_END;
    scArrowUp.rotation = 180;
  }else {
    endY = PC_Y_START;
    remEndY = WTB_Y_START;
    scArrowUp.rotation = 0;
  }

  prodCategories.animate({
    properties: { y:endY },
    curve: animationCurve
  });  

  whereToBuy.animate({
    properties: { y:remEndY },
    curve: animationCurve
  });  

});

directionsLink.on("click", function() {

  var endY,
      remEndY,
      rotation,
      textOpen;

	textForm.y = ROW_Y_START;
	tlArrowUp.rotation = 0;
  textOpen = (state === 'text_open') ? true : false;

  if (directionsForm.y === ROW_Y_START) {
    endY = ROW_Y_END;
    remEndY = REM_Y_END;
    dlArrowUp.rotation = 180;
    state = 'dir_open';
  }else {
    endY = ROW_Y_START;
    remEndY = REM_Y_START;
    dlArrowUp.rotation = 0;
    state = 'dir_closed';
  }

  if (! textOpen) {
  	resultsRemaining.animate({
  		properties: { y:remEndY },
  		curve: animationCurve
  	});
  }

	directionsForm.animate({
		properties: { y:endY },
		curve: animationCurve
	});

});

textLink.on("click", function() {

  var endY,
      remEndY,
      rotation,
      dirOpen;

	directionsForm.y = ROW_Y_START;
	dlArrowUp.rotation = 0;
  dirOpen = (state === 'dir_open') ? true : false;

  if (textForm.y === ROW_Y_START) {
    endY = ROW_Y_END;
    remEndY = REM_Y_END;
    tlArrowUp.rotation = 180;
    state = 'text_open';
  }else {
    endY = ROW_Y_START;
    remEndY = REM_Y_START;
    tlArrowUp.rotation = 0;
    state = 'text_closed';
  }

  if (! dirOpen) {
    resultsRemaining.animate({
      properties: { y:remEndY },
      curve: animationCurve
    });   
  }

	textForm.animate({
		properties: { y:endY },
		curve: animationCurve
	});

});