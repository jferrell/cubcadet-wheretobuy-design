var SLIDE_Y_START = 438,
    SLIDE_Y_END = 911,
    MAP_Y_START = -920,
    MAP_Y_END = 0

var animationCurve = "spring(600,40,500)";

var filterToggle = PSD.filter_toggle,
    filterArrow = PSD.ft_arrow,
    slideRemaining = PSD.slide,
    productCategories = PSD.product_categories,
    mapLoading = PSD.map_loading,
    mapViewBtn = PSD.map_view,
    mapCloseBtn = PSD.close_map_btn,
    map = PSD.map,   
    currentLocation = PSD.current_location,
    currentLocationActive = PSD.current_location_active,
    zipLocation = PSD.zip_location,
    zipLocationActive = PSD.zip_location_active


productCategories.style["z-index"] = 1;
slideRemaining.style["z-index"] = 2;
mapLoading.opacity = 0;
mapLoading.scale = 0;
map.opacity = 0;
map.scale = 0;
currentLocationActive.opacity = 0;
zipLocation.opacity = 0;

currentLocation.on("click", function() {
  currentLocation.opacity = 0;
  zipLocationActive.opacity = 0;
  zipLocation.opacity = 1;
  currentLocationActive.opacity = 1;
});

zipLocationActive.on("click", function() {
  zipLocation.opacity = 0;
  currentLocationActive.opacity = 0;
  currentLocation.opacity = 1;
  zipLocationActive.opacity = 1;
});

filterToggle.on("click", function() {
  
  if (slideRemaining.y === SLIDE_Y_START) {
    endY = SLIDE_Y_END;
    filterArrow.rotation = 180;
  }else {
    endY = SLIDE_Y_START;
    filterArrow.rotation = 0;
  }

  slideRemaining.animate({
    properties: { y: endY },
    curve: animationCurve
  }); 

});

mapViewBtn.on("click", function() {
  
  mapLoading.animate({
    properties: { scale: 1, opacity: 1 },
    curve: animationCurve
  }); 

  utils.delay(1000, function() {
    map.animate({
      properties: { scale: 1, opacity: 1 },
      curve: animationCurve
    });   
  });

});

mapCloseBtn.on("click", function() {

  map.opacity = 0;
  map.scale = 0;
  
  mapLoading.animate({
    properties: { scale: 0, opacity: 0 },
    curve: animationCurve
  }); 

});