# Important Note

Please use Chrome when viewing the links below.

# Full Site - Interactive Design Preview Site

The following elements are clickable and interactive on the interactive page:

* Show Categories Toggle
* Map View Toggle
* Map Pin #1 Toggle 
* Map Pin Dialog (Get Directions, Text Address, Close Icon)
* List View: Get Directions (only 1st row Get Directions is interactive)
* List View: Text Address (only 1st row Text Directions is interactive)

[View Full Site - Interactive Design Preview Site](http://cubcadet-wheretobuy.herokuapp.com/transitions/index.html)

# Mobile Site - Interactive Design Preview Site

The designs are shown at 2x the physical size due to the PSD being built to support retina/high-denisity device screens.

You can scroll the actual content, just as you would on a real mobile device, by having your cursor within the viewport of the phone (e.g. where the content is).

The following elements are clickable and interactive on the interactive page:

* Zip code input box 
* Current Location checkbox
* Show Categories Toggle
* Map View Button
* Close Map Button

[View Mobile Site - Interactive Design Preview Site](http://cubcadet-wheretobuy.herokuapp.com/transitions/mobile/index.html)














