require 'sinatra'

get '/' do
  @filename = params[:filename] || '01-default'
  @height = params['h'] || 2059
  erb :design, :layout => !request.xhr?
end

get '/:filename' do
  @filename = params[:filename]
  @height = params['h']
  @overlay = params['o']
  @checkout = params['c']
  erb :design, :layout => !request.xhr?
end